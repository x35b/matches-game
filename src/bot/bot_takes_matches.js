import { game_mods } from '../settings'

export default (matches_remain, game_mod) => {
    if (game_mod === game_mods.botfirst.id && matches_remain === 25) return Math.ceil(Math.random() * 3)
    else if (matches_remain === 1 || (matches_remain - 1) % 4 === 0 || (matches_remain - 1) % 4 === 1) return 1
    else return 3
}