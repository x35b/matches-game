import React, { Component } from 'react';
import Button from './components/button/button'
import bot_takes_matches from './bot/bot_takes_matches'
import matchsvg from './match.svg'
import { Container, Grid, Box, Radio, RadioGroup, FormControlLabel, FormControl } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { DefaultSettings, game_mods } from './settings'

class App extends Component {
    state = new DefaultSettings()

    playerMoveHandler = (matches_taken_by_player) => {
        let matches = { ...this.state.matches }

        const finish_game = (matches) => {
            this.setState({
                matches: matches,
                gameover: true,
                win_alert_showed: matches.player_has % 2 ? false : true
            })
        }

        matches.remain -= matches_taken_by_player
        matches.player_has += matches_taken_by_player
        if (matches.remain <= 0) return finish_game(matches)

        const matches_taken_by_bot = bot_takes_matches(matches.remain, this.state.game_mod) // returns how many matches bot has taken
        matches.remain -= matches_taken_by_bot
        matches.bot_has += matches_taken_by_bot
        if (matches.remain <= 0) return finish_game(matches)

        this.setState({ matches: matches })
    }

    startNewGameHandler = () => {
        let new_settings = new DefaultSettings()
        if (this.state.game_mod === game_mods.playerfirst.id) {
            return this.setState(new_settings)
        }
        else if (this.state.game_mod === game_mods.botfirst.id) {
            new_settings.game_mod = game_mods.botfirst.id
            const matches_taken_by_bot = bot_takes_matches(new_settings.matches.remain, new_settings.game_mod)
            new_settings.matches.remain -= matches_taken_by_bot
            new_settings.matches.bot_has += matches_taken_by_bot
            return this.setState(new_settings)
        }
    }

    changeGameMod = (event) => {
        let new_settings = new DefaultSettings()
        new_settings.game_mod = event.target.value
        if (new_settings.game_mod === game_mods.botfirst.id) {
            const matches_taken_by_bot = bot_takes_matches(new_settings.matches.remain, new_settings.game_mod)
            new_settings.matches.remain -= matches_taken_by_bot
            new_settings.matches.bot_has += matches_taken_by_bot
        }
        this.setState(new_settings)
    }

    render() {
        let buttons
        if (this.state.gameover) buttons = <Button onclick={this.startNewGameHandler}>Try Again</Button>
        else {
            buttons = this.state.buttons.map((val, index) => {
                index++
                const disabled_state = index > this.state.matches.remain ? true : false
                return (
                    <Button
                        key={index}
                        index={index}
                        onclick={() => { this.playerMoveHandler(index) }}
                        disabled={disabled_state}>
                        Take {index} match{index > 1 ? 'es' : null}
                    </Button>
                )
            })
        }

        return (
            <Container maxWidth="lg" className='text-white'>
                {this.state.win_alert_showed ? <Box my={2}><Alert severity="success" variant="filled">You Win!</Alert></Box> : null}

                <Box my={2}>
                    <FormControl component="fieldset">
                        <RadioGroup row value={this.state.game_mod} onChange={this.changeGameMod}>
                            {Object.values(game_mods).map(mod => <FormControlLabel key={mod.id} value={mod.id} control={<Radio />} label={mod.text} />)}
                        </RadioGroup>
                    </FormControl>
                </Box>

                <Box my={2} textAlign="center">{buttons}</Box>

                <Box fontSize="h2.fontSize" my={2}>Matches remain: {this.state.matches.remain}</Box>

                <Grid container spacing={3}>
                    <Grid item xs={12} lg={6}>
                        <Box borderRight={1}>
                            <Box fontSize='h4.fontSize' mb={2}>Player has {this.state.matches.player_has} matches</Box>
                            {new Array(this.state.matches.player_has).fill(0).map((val, index) => <img className='matchsvg' src={matchsvg} key={index} alt='' />)}
                        </Box>
                    </Grid>
                    <Grid item xs={12} lg={6}>
                        <Box fontSize='h4.fontSize' mb={2}>Bot has {this.state.matches.bot_has} matches</Box>
                        {new Array(this.state.matches.bot_has).fill(0).map((val, index) => <img className='matchsvg' src={matchsvg} key={index} alt='' />)}
                    </Grid>
                </Grid>
            </Container >
        )
    }
}

export default App;
