import React from 'react'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'

export default props => {
    return (
        <Box m={2} display='inline-block'>
            <Button onClick={props.onclick} disabled={props.disabled} variant='contained' color='primary' size='large'>{props.children}</Button>
        </Box>
    )
}