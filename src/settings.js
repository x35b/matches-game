class Default {
    game_mod = 'playerfirst'
    matches = {
        remain: 25,
        player_has: 0,
        bot_has: 0
    }
    buttons = new Array(3).fill(0)
    gameover = false
    win_alert_showed = false
}

const game_mods = {
    playerfirst: {
        id: 'playerfirst',
        text: 'Player First Turn'
    },
    botfirst: {
        id: 'botfirst',
        text: 'Bot First Turn'
    }
}

export { Default as DefaultSettings, game_mods }